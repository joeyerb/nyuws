angular.module('nyuws.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, News) {
  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };

  // Update: fetch all unread items
  $scope.updateAll = function() {
      News.getFolders(function(data) {
          console.log('Folders updated');
      });
      News.getFeeds(function(data) {
          console.log('Feeds updated');
      });
      var params = {
          "batchSize": 100,
          "type": 3,
          "id": 0,
          "getRead": true
      };
      News.getItems(params, function(data) {
          console.log('Items updated');
      });
  };

  $scope.updateAll();
})

//// 1 Folders
.controller('FoldersCtrl', function($scope, News) {
    News.getFolders(function(data) {
        $scope.folders = data.folders;
        console.log('folders', JSON.stringify($scope.folders));
    });
})

//// 2 Feeds
// 2.1 All feeds
.controller('FeedsCtrl', function($scope, News) {
    News.getFeeds(function(data) {
        $scope.feeds = data.feeds;
    });
})

// 2.2 Feeds in folder
.controller('FolderCtrl', function($scope, $stateParams, News) {

    var folderId = parseInt($stateParams.folderId);
    $scope.feeds = [];

    $scope.folderName = News.getFolderName(folderId);

    News.getFeeds(function(data) {
        for (var i in data.feeds) {
            if (data.feeds[i].folderId === folderId)
                $scope.feeds.push(data.feeds[i]);
        }
        console.log('folder', JSON.stringify($scope.feeds));
    });
})

//// 3 Items
// 3.1 All items
.controller('ItemsCtrl', function($scope, News) {
    var params = {
        "batchSize": 10,
        "type": 3,
        "id": 0,
        "getRead": true
    };
    News.getItems(params, function(data) {
        $scope.items = data.items;
    });
})
// 3.2 Items in feed
.controller('FeedCtrl', function($scope, $stateParams, News) {
    var feedId = parseInt($stateParams.feedId);
    var params = {
        "batchSize": 10,
        "type": 0,
        "id": feedId,
        "getRead": true
    };
    console.log('feed', JSON.stringify(params));
    News.getItems(params, function(data) {
        $scope.items = data.items;
    });
})
// 3.3 Starred items
.controller('FavouriteCtrl', function($scope, News) {
    var params = {
        "batchSize": 10,
        "type": 2,
        "id": 0,
        "getRead": true
    };
    News.getItems(params, function(data) {
        $scope.items = data.items;
    });
})

//// 4 Article
.controller('ArticleCtrl', function($scope, $stateParams, News) {
    var itemId = parseInt($stateParams.itemId);
    console.log('itemId', itemId);
    News.getItem(itemId, function(data, prevId, nextId) {
        data.pubDate = new Date(data.pubDate*1000).toISOString().substring(0,19).replace('T', ' ');
        $scope.item = data;
        $scope.prevId = prevId;
        $scope.nextId = nextId;
    });
})


;
