// angular.module is a global place for creating, registering and retrieving Angular modules
angular.module('nyuws', ['ionic', 'nyuws.controllers', 'nyuws.services'])

.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
})

.config(function($stateProvider, $urlRouterProvider, $httpProvider, $provide) {
    $provide.factory('AuthInterceptor', ['Credentials', '$q', function (Credentials, $q) {
        return {
            request: function (config) {
                // only set auth headers if url matches the api url
                if(config.url.indexOf(Credentials.url) === 0) {
                    auth = btoa(Credentials.userName + ':' + Credentials.password);
                    config.headers['Authorization'] = 'Basic ' + auth;
                }
                return config || $q.when(config);
            }
        };
    }]);
    $httpProvider.interceptors.push('AuthInterceptor');

    $stateProvider
        .state('app', {
            url: "/app",
            abstract: true,
            templateUrl: "templates/menu.html",
            controller: 'AppCtrl'
        })

    // 1 Folders
    .state('app.folders', {
        url: "/folders",
        views: {
            'menuContent': {
                templateUrl: "templates/folders.html",
                controller: 'FoldersCtrl'
            }
        }
    })

    // 2 Feeds
    // 2.1 All Feeds
    .state('app.feeds', {
        url: "/feeds",
        views: {
            'menuContent': {
                templateUrl: "templates/feeds.html",
                controller: 'FeedsCtrl'
            }
        }
    })
    // 2.2 Feeds in Folder
    .state('app.folder', {
        url: "/folders/:folderId",
        views: {
            'menuContent': {
                templateUrl: "templates/feeds.html",
                controller: 'FolderCtrl'
            }
        }
    })

    // 3 Items
    // 3.1 All Items
    .state('app.items', {
        url: "/items",
        views: {
            'menuContent': {
                templateUrl: "templates/items.html",
                controller: 'ItemsCtrl'
            }
        }
    })
    // 3.2 Items in Feed
    .state('app.feed', {
        url: "/feeds/:feedId",
        views: {
            'menuContent': {
                templateUrl: "templates/items.html",
                controller: 'FeedCtrl'
            }
        }
    })
    // 3.3 Starred Items
    .state('app.favourite', {
        url: "/favourite",
        views: {
            'menuContent': {
                templateUrl: "templates/items.html",
                controller: 'FavouriteCtrl'
            }
        }
    })

    // 4 Item Content
    .state('app.article', {
        url: "/items/:itemId",
        views: {
            'menuContent': {
                templateUrl: "templates/article.html",
                controller: 'ArticleCtrl'
            }
        }
    });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/folders');
});
