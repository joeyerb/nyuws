// Local Storage
angular.module('ionic.utils', [])

.factory('$localstorage', ['$window', function($window) {
    return {
        set: function(key, value) {
            $window.localStorage[key] = value;
        },
        get: function(key, defaultValue) {
            return $window.localStorage[key] || defaultValue;
        },
        setObject: function(key, value) {
            $window.localStorage[key] = JSON.stringify(value);
        },
        getObject: function(key) {
            return JSON.parse($window.localStorage[key] || '{}');
        }
    }
}]);

// App Services
angular.module('nyuws.services', [])

.factory('Credentials', function () {
    // TODO
    return {
        userName: 'nyuws',
        password: 'swuyn',
        url: 'http://127.0.0.1:8004' + '/index.php/apps/news/api/v1-2'
    };
})

// ownCloud News API 1.2
.factory('News', function($http, Credentials) {
    var cache = {};
    return {

        //////////////////  Folders  ////////////////
        // Get all folders, return {"folders": Array}
        getFolders: function(callback) {
            console.log('gggggggggg');
            $http.get(Credentials.url + '/folders')
                .success(function(data) {
                    cache.folders = data.folders;
                    callback(data);
                })
                .error(function(data, status, headers, config) {
                    console.log('ERROR: GET /folders', data, status, headers, JSON.stringify(config));
                });
        },
        getFolderName: function(folderId) {
            console.log('get ffff', JSON.stringify(folderId));
            for (var i in cache.folders)
                if (cache.folders[i].id === folderId)
                    return cache.folders[i].name;
            return "Unknown Folder";
        },

        // Create a folder, return {"folders": Array}
        createFolder: function(folderName, callback) {
            $http.post(Credentials.url + 'folders', {"name": folderName})
                .success(callback)
                .error(function(data, status, headers, config) {
                    console.log('ERROR: POST /folders', data, status, headers, config);
                    if (status === 409)
                        console.log('HTTP 409: The folder exists already.');
                    if (status === 422)
                        console.log('HTTP 422: The folder name is invalid.');
                });
        },

        // Delete a folder, return nothing
        deleteFolder: function(folderId, callback) {
            $http.delete(Credentials.url + '/folders/' + folderId)
                .success(callback)
                .error(function(data, status, headers, config) {
                    console.log('ERROR: DELETE /folders/{folderId}', data, status, headers, config);
                    if (status === 404)
                        console.log('HTTP 404: The folder does not exist.');
                });
        },

        // Rename a folder, return nothing
        renameFolder: function(folderId, folderName, callback) {
            $http.put(Credentials.url + '/folders/' + folderId, {"name": folderName})
                .success(callback)
                .error(function(data, status, headers, config) {
                    console.log('ERROR: PUT /folders/{folderId}', data, status, headers, config);
                    if (status === 409)
                        console.log('HTTP 409: The folder name does already exist');
                    if (status === 422)
                        console.log('HTTP 422: The folder name is invalid');
                    if (status === 404)
                        console.log('HTTP 404: The folder does not exist.');
                });
        },

        // Mark items of a folder as read, return nothing
        markFolder: function(folderId, callback) {
            $http.put(Credentials.url + '/folders/' + folderId + '/read')
                .success(callback)
                .error(function(data, status, headers, config) {
                    console.log('ERROR: PUT /folders/{folderId}', data, status, headers, config);
                    if (status === 404)
                        console.log('HTTP 404: The folder does not exist.');
                });
        },

        //////////////////  Feeds  ////////////////
        // Get all feeds
        getFeeds: function(callback) {
            $http.get(Credentials.url + '/feeds')
                .success(function(data) {
                    cache.feeds = data.feeds;
                    callback(data);
                })
                .error(function(data, status, headers, config) {
                    console.log('ERROR: GET /feeds', data, status, headers, config);
                });
        },
        // Create a feed

        // Delete a feed

        // Move a feed to a different folder

        // Rename a feed

        // Mark items of a feed as read


        //////////////////  Items  ////////////////
        // Get items, return {"items": Array}
        getItems: function(params, callback) {
            $http({
                method: 'GET',
                url: Credentials.url + '/items',
                params: params
            })
            .success(function(data) {
                cache.news = data.items;
                callback(data);
            })
            .error(function(data, status, headers, config) {
                console.log('ERROR: GET /items', data, status, headers, config);
            });
        },
        // Get updated items

        //////////////////  Article  ////////////////
        // Get item from cache.news
        getItem: function(itemId, callback) {
            var prevId=itemId, nextId=itemId;
            for (var i in cache.news)
                if (cache.news[i].id === itemId) {
                    if (i-1 >= 0)
                        prevId = cache.news[i-1].id;
                    if (i+1 < cache.news.length)
                        nextId = cache.news[i+1].id;
                    callback(cache.news[i], prevId, nextId);
                }
        }
    };
});
